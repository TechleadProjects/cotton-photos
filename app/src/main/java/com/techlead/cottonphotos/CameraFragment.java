package com.techlead.cottonphotos;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.Camera;
import android.hardware.camera2.CameraDevice;
import android.net.ConnectivityManager;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Environment;
import android.util.Size;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.ListObjectsRequest;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.github.dhaval2404.imagepicker.ImagePicker;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.apache.commons.io.FilenameUtils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Type;
import java.nio.channels.FileChannel;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

public class CameraFragment extends Fragment {

    private Context context;
    private LinearLayout layParent;
    private FloatingActionButton captureImageButton;
    private ImageView capturedImage;
    private String profileByteString, displayName;
    private ProgressDialog progDailog;
    private ArrayList<ImageData> filePathArrayList;


    private int uploadCount = 0;

    public CameraFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_camera, container, false);
        context = getActivity();
        filePathArrayList = new ArrayList<>();

        captureImageButton = view.findViewById(R.id.captureImageButton);
        capturedImage = view.findViewById(R.id.capturedImage);

        captureImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                SharedPreferences sharedPreferences = context.getSharedPreferences("User", 0);
                String username = sharedPreferences.getString("username", "");
                String category = sharedPreferences.getString("category", "");
                String photoNamePrefix = sharedPreferences.getString("photoNamePrefix", "");

                if (username.equals("") || category.equals("") || photoNamePrefix.equals("")) {

                    new AlertDialog.Builder(context)
                            .setTitle("Cotton Photos")
                            .setCancelable(false)
                            .setMessage("Please fill the user data in Settings tab before uploading the picture.")
                            .setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            }).show();

                } else {

                    ImagePicker.Companion.with(getActivity()).cameraOnly().compress(5120).maxResultSize(1740,1740).start();
                }
            }
        });

//        if (checkConnection(context)) {
//            SharedPreferences sharedPreferences = context.getSharedPreferences("User", 0);
//
//            String storedUsername = sharedPreferences.getString("username", "");
//            String storedCategory = sharedPreferences.getString("category", "");
//
//            if (!storedUsername.equals("") && !storedCategory.equals("")) {
//                String filePathsString = sharedPreferences.getString(storedUsername.concat("_").concat(storedCategory).concat("images"), "");
//
//                if (!filePathsString.equals("")) {
//
//                    Type type = new TypeToken<ArrayList<String>>() {
//                    }.getType();
//                    Gson gson = new Gson();
//                    filePathArrayList = gson.fromJson(filePathsString, type);
//
//                    syncDataWithServer(filePathArrayList);
//                }
//            }
//        }

        return view;
    }

//    private void syncDataWithServer(final ArrayList<String> filePathArrayList) {
//
//        Thread t1 = new Thread(new Runnable() {
//            @Override
//            public void run() {
//
//                uploadCount = 0;
//
//                getActivity().runOnUiThread(new Runnable() {
//                    public void run() {
//                        progDailog = new ProgressDialog(context);
//                        progDailog.setMessage("Uploading files to server...");
//                        progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
//                        progDailog.setCancelable(false);
//                        progDailog.setIndeterminate(false);
//                        progDailog.show();
//                    }
//                });
//
//                for (int i=0; i<filePathArrayList.size(); i++) {
//
//                    if (filePathArrayList.get(i) == null) {
//                        continue;
//                    }
//
//                    String[] pathArray = filePathArrayList.get(i).split("/");
//                    String displayName = pathArray[pathArray.length - 1];
//
//                    SimpleDateFormat ft = new SimpleDateFormat ("yyyy.MM.dd-hh:mm:ss:SSS");
//                    String ext = FilenameUtils.getExtension(displayName);
//
//                    SharedPreferences sharedPreferences = context.getSharedPreferences("User", 0);
//                    String photoNamePrefix = sharedPreferences.getString("photoNamePrefix", "");
//                    String category = sharedPreferences.getString("category", "");
//
//                    if (photoNamePrefix.equals("")) {
//                        photoNamePrefix = "CottonPhotos";
//                    }
//
//                    if (category.equals("")) {
//                        category = "General";
//                    }
//
//                    final String fileName = category.concat("_").concat(photoNamePrefix).concat("_").concat(ft.format(new Date()));
//
//                    boolean status = uploadFilesToS3(filePathArrayList.get(i), fileName, ext);
//
//                    if (status) {
//
//                        uploadCount++;
//                    }
//                }
//
//                getActivity().runOnUiThread(new Runnable() {
//                    public void run() {
//
//                        if (progDailog != null) {
//                            progDailog.dismiss();
//                        }
//
//                    }
//                });
//
//                if (uploadCount == filePathArrayList.size()) {
//
//                    SharedPreferences sharedPreferences = context.getSharedPreferences("User", 0);
//                    String username = sharedPreferences.getString("username", "");
//                    String category = sharedPreferences.getString("category", "");
//
//                    String keyForImages = username.concat("_").concat(category).concat("images");
//
//                    sharedPreferences.edit().putString(keyForImages, "").commit();
//
//                    getActivity().runOnUiThread(new Runnable() {
//                        public void run() {
//
//                            Toast.makeText(context, "Data synced successfully with server.", Toast.LENGTH_SHORT).show();
//                        }
//                    });
//
//                } else {
//
//                    getActivity().runOnUiThread(new Runnable() {
//                        public void run() {
//
//                            Toast.makeText(context, "Error occurred while uploading data to server. Please try again later.", Toast.LENGTH_SHORT).show();
//                        }
//                    });
//                }
//            }
//        });
//        t1.start();
//    }
    public void copy(File src, File dst) throws IOException {
        FileInputStream inStream = new FileInputStream(src);
        FileOutputStream outStream = new FileOutputStream(dst);
        FileChannel inChannel = inStream.getChannel();
        FileChannel outChannel = outStream.getChannel();
        inChannel.transferTo(0, inChannel.size(), outChannel);
        inStream.close();
        outStream.close();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {

            capturedImage.setImageURI(data.getData());

            //You can get File object from intent
            File file = ImagePicker.Companion.getFile(data);


            if(file != null) {
                String tempImagePath = file.getPath();

                String[] tempPathArray = tempImagePath.split("/");
                String tempDisplayName = tempPathArray[tempPathArray.length - 1];

                Bitmap b = BitmapFactory.decodeFile(file.getAbsolutePath());
                File editedFile = getCustomResImage(b, tempDisplayName);

                profileByteString = editedFile.getPath();
                String[] pathArray = profileByteString.split("/");
                displayName = pathArray[pathArray.length - 1];

                SimpleDateFormat ft = new SimpleDateFormat ("HHmmss");
                final String ext = FilenameUtils.getExtension(displayName);

                SharedPreferences sharedPreferences = context.getSharedPreferences("User", 0);
                String username = sharedPreferences.getString("username", "");
                String category = sharedPreferences.getString("category", "");
                String photoNamePrefix = sharedPreferences.getString("photoNamePrefix", "");

                if (photoNamePrefix.equals("")) {
                    photoNamePrefix = "CottonPhotos";
                }

                if (category.equals("")) {
                    category = "General";
                }


                String keyForImages = "imagepath";

                //temp
                String filePathsString = sharedPreferences.getString(keyForImages, "");
                final String fileName = photoNamePrefix.concat("_").concat(category).concat("_").concat(ft.format(new Date())).replace(" ", "_");
                // Save File Again

                try {
                    File dir = new File(context.getExternalFilesDir(null), username);
                    if (!dir.exists()) {
                        dir.mkdirs();
                    }
                    File f = new File(context.getExternalFilesDir(null) + "/" + username, fileName + "." + ext);
                    copy(file, f);
                }catch(Exception e)
                {
                    Toast.makeText(context,"Error saving a file in a backup folder",Toast.LENGTH_LONG).show();
                }

                Gson gson = new Gson();
                if (filePathsString.equals("")) {

                    filePathArrayList = new ArrayList<>();


                    filePathArrayList.add(new ImageData(fileName,profileByteString));

                    String filePaths = gson.toJson(filePathArrayList);

                    sharedPreferences.edit().putString(keyForImages, filePaths).commit();

                } else {

                    Type type = new TypeToken<ArrayList<ImageData>>(){}.getType();
                    filePathArrayList = gson.fromJson(filePathsString, type);

                    filePathArrayList.add(new ImageData(fileName,profileByteString));
                    String filePaths = gson.toJson(filePathArrayList);

                    sharedPreferences.edit().putString(keyForImages, filePaths).commit();
                }

                Toast.makeText(context, "Photo has been stored successfully.", Toast.LENGTH_SHORT).show();

//                if (checkConnection(context)) {
//
//                    SimpleDateFormat ft1 = new SimpleDateFormat ("yyyy-MM-dd HH:mm:ss");
//                    String lastFailedTime = sharedPreferences.getString(username.concat("_").concat(category).concat("_").concat("lastUploadFailedTime"), "");
//
//                    if (!lastFailedTime.equals("")) {
//
//                        try {
//
//                            Date failedDateTime = ft1.parse(lastFailedTime);
//                            Date currentDate = new Date();
//
//                            if (failedDateTime != null) {
//
//                                long difference = currentDate.getTime() - failedDateTime.getTime();
//                                long diffInMinutes = difference / (60 * 1000);
//
//                                if (diffInMinutes < 10) {
//
//                                    String filePathsString = sharedPreferences.getString(keyForImages, "");
//
//                                    Gson gson = new Gson();
//                                    if (filePathsString.equals("")) {
//
//                                        filePathArrayList = new ArrayList<>();
//                                        filePathArrayList.add(profileByteString);
//
//                                        String filePaths = gson.toJson(filePathArrayList);
//
//                                        sharedPreferences.edit().putString(keyForImages, filePaths).commit();
//
//                                    } else {
//
//                                        Type type = new TypeToken<ArrayList<String>>(){}.getType();
//                                        filePathArrayList = gson.fromJson(filePathsString, type);
//
//                                        filePathArrayList.add(profileByteString);
//                                        String filePaths = gson.toJson(filePathArrayList);
//
//                                        sharedPreferences.edit().putString(keyForImages, filePaths).commit();
//                                    }
//
//                                    Toast.makeText(context, "Photo stored locally as there is no internet connection.", Toast.LENGTH_SHORT).show();
//                                    return;
//                                }
//                            }
//
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                        }
//                    }
//
//                    Thread t1 = new Thread(new Runnable() {
//                        @Override
//                        public void run() {
//
//                            getActivity().runOnUiThread(new Runnable() {
//                                public void run() {
//                                    progDailog = new ProgressDialog(context);
//                                    progDailog.setMessage("Uploading file to server...");
//                                    progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
//                                    progDailog.setCancelable(false);
//                                    progDailog.setIndeterminate(false);
//                                    progDailog.show();
//                                }
//                            });
//
//                            boolean status = uploadFilesToS3(profileByteString, fileName, ext);
//
//                            if (status) {
//
//                                getActivity().runOnUiThread(new Runnable() {
//                                    public void run() {
//
//                                        if (progDailog != null) {
//                                            progDailog.dismiss();
//                                        }
//
//                                    }
//                                });
//
//                                getActivity().runOnUiThread(new Runnable() {
//                                    public void run() {
//
//                                        Toast.makeText(context, "Photo saved successfully on server.", Toast.LENGTH_SHORT).show();
//                                    }
//                                });
//
//                            } else {
//
//                                getActivity().runOnUiThread(new Runnable() {
//                                    public void run() {
//
//                                        if (progDailog != null) {
//                                            progDailog.dismiss();
//                                        }
//
//                                    }
//                                });
//
//                                getActivity().runOnUiThread(new Runnable() {
//                                    public void run() {
//
////                                    new AlertDialog.Builder(context)
////                                            .setTitle("Cotton Photos")
////                                            .setCancelable(false)
////                                            .setMessage("Error occurred while uploading the picture to server. Please try again later.")
////                                            .setPositiveButton("Okay", new DialogInterface.OnClickListener() {
////                                                @Override
////                                                public void onClick(DialogInterface dialog, int which) {
////                                                    dialog.dismiss();
////                                                }
////                                            }).show();
//                                    }
//                                });
//                            }
//                        }
//                    });
//                    t1.start();
//
//                } else {
//
//                    String filePathsString = sharedPreferences.getString(keyForImages, "");
//
//                    Gson gson = new Gson();
//                    if (filePathsString.equals("")) {
//
//                        filePathArrayList = new ArrayList<>();
//                        filePathArrayList.add(profileByteString);
//
//                        String filePaths = gson.toJson(filePathArrayList);
//
//                        sharedPreferences.edit().putString(keyForImages, filePaths).commit();
//
//                    } else {
//
//                        Type type = new TypeToken<ArrayList<String>>(){}.getType();
//                        filePathArrayList = gson.fromJson(filePathsString, type);
//
//                        filePathArrayList.add(profileByteString);
//                        String filePaths = gson.toJson(filePathArrayList);
//
//                        sharedPreferences.edit().putString(keyForImages, filePaths).commit();
//                    }
//
//                    Toast.makeText(context, "Photo stored locally as there is no internet connection.", Toast.LENGTH_SHORT).show();
//                }

            } else {
                Toast.makeText(context, "Could not select Image.", Toast.LENGTH_SHORT).show();
            }

        } else if (resultCode == ImagePicker.RESULT_ERROR) {
            Toast.makeText(context, "Could not select Image.", Toast.LENGTH_SHORT).show();
        }
    }

//    public boolean uploadFilesToS3(String filePath, String fileName, String ext){
//
//        try {
//
//            ClientConfiguration clientConfiguration = new ClientConfiguration()
//                    .withMaxErrorRetry(1) // 1 retries
//                    .withConnectionTimeout(200000) // 30,000 ms
//                    .withSocketTimeout(200000); // 30,000 ms
//
//            AmazonS3 s3Client = new AmazonS3Client(new BasicAWSCredentials("AKIAJVLYTY7JZ5HHOCTQ", "v2IuBHv/ritpqr0/pbPCKqxt7XBWH6bLGrIEmupD"), clientConfiguration);
//            s3Client.setRegion(Region.getRegion(Regions.AP_SOUTH_1));
//
//            SharedPreferences sharedPreferences = context.getSharedPreferences("User", 0);
//            String username = sharedPreferences.getString("username", "");
//            String photoNamePrefix = sharedPreferences.getString("photoNamePrefix", "");
//            String category = sharedPreferences.getString("category", "");
//            String phoneNumber = sharedPreferences.getString("phoneNumber", "");
//            String cityName = sharedPreferences.getString("cityName", "");
//
//            String folderName = "";
//
//            if (username.equals("")) {
//                folderName = "CottonPhotos";
//            } else {
//                folderName = username;
//            }
//
//            ListObjectsRequest listObjectsRequest = new ListObjectsRequest()
//                    .withBucketName("farmai")
//                    .withPrefix(folderName + "/");
//
//            ObjectListing objectListing = s3Client.listObjects(listObjectsRequest);
//
//            int count = 0;
//            Date lastModifiedDate = null;
//            Date latestModifiedDate = null;
//            Date currentDate = new Date();
//            String objectNumber = "";
//
//            Calendar cal1 = Calendar.getInstance();
//            Calendar cal2 = Calendar.getInstance();
//            cal1.setTimeZone(TimeZone.getDefault());
//            cal2.setTimeZone(TimeZone.getDefault());
//
//            cal2.setTime(currentDate);
//
//            for (S3ObjectSummary summary : objectListing.getObjectSummaries()) {
//
//                if (lastModifiedDate == null) {
//
//                    lastModifiedDate = summary.getLastModified();
//                } else if (lastModifiedDate.before(summary.getLastModified())){
//
//                    lastModifiedDate = summary.getLastModified();
//                }
//
//                latestModifiedDate = summary.getLastModified();
//                cal1.setTime(latestModifiedDate);
//
//                boolean sameDay = cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR) &&
//                        cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR);
//
//                if (sameDay) {
//                    count++ ;
//                }
//            }
//
//            if (lastModifiedDate != null) {
//                cal1.setTime(lastModifiedDate);
//                cal2.setTime(currentDate);
//                boolean sameDay = cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR) &&
//                        cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR);
//
//                objectNumber = String.format(Locale.getDefault(), "%04d", count);
//
//                if (!sameDay) {
//
//                    objectNumber = "0000";
//                }
//            } else {
//                objectNumber = "0000";
//            }
//            Map<String,String> map = new HashMap<>();
//            map.put("U",username);
//            map.put("P",photoNamePrefix);
//            map.put("N",phoneNumber);
//            map.put("C",category);
//            map.put("L",cityName);
//
//            PutObjectRequest por = new PutObjectRequest("farmai", folderName.concat("/").concat(fileName.concat("_").concat(objectNumber).concat(".").concat(ext)), new File(filePath));
//            por.withCannedAcl(CannedAccessControlList.PublicRead);
//            ObjectMetadata metadata = new ObjectMetadata();
//            metadata.setUserMetadata(map);
//            por.withMetadata(metadata);
//            s3Client.putObject(por);
//
//            return true;
//        } catch (Exception e){
//
//            SharedPreferences sharedPreferences = context.getSharedPreferences("User", 0);
//            String username = sharedPreferences.getString("username", "");
//            String category = sharedPreferences.getString("category", "");
//
//            String keyForImages = username.concat("_").concat(category).concat("images");
//            String filePathsString = sharedPreferences.getString(keyForImages, "");
//
//            Gson gson = new Gson();
//            if (filePathsString.equals("")) {
//
//                filePathArrayList = new ArrayList<>();
//                filePathArrayList.add(profileByteString);
//
//                String filePaths = gson.toJson(filePathArrayList);
//
//                sharedPreferences.edit().putString(keyForImages, filePaths).commit();
//
//            } else {
//
//                Type type = new TypeToken<ArrayList<String>>(){}.getType();
//                filePathArrayList = gson.fromJson(filePathsString, type);
//
//                filePathArrayList.add(profileByteString);
//                String filePaths = gson.toJson(filePathArrayList);
//
//                sharedPreferences.edit().putString(keyForImages, filePaths).commit();
//            }
//
//            SimpleDateFormat ft = new SimpleDateFormat ("yyyy-MM-dd HH:mm:ss");
//            sharedPreferences.edit().putString(username.concat("_").concat(category).concat("_").concat("lastUploadFailedTime"), ft.format(new Date())).commit();
//
//            getActivity().runOnUiThread(new Runnable() {
//                public void run() {
//
//                    Toast.makeText(context, "Photo stored locally as there is no internet connection.", Toast.LENGTH_SHORT).show();
//                }
//            });
//
//            return false;
//        }
//    }

    public boolean checkConnection(Context context) {
        boolean flag = false;
        try {
            ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            if(connectivityManager != null){
                NetworkInfo info = connectivityManager.getActiveNetworkInfo();

                if(info != null){
                    if (info.getType() == ConnectivityManager.TYPE_WIFI) {
                        System.out.println(info.getTypeName());
                        flag = true;
                    }
                    if (info.getType() == ConnectivityManager.TYPE_MOBILE) {
                        System.out.println(info.getTypeName());
                        flag = true;
                    }
                }
            }

        } catch (Exception exception) {
            System.out.println("Exception at network connection....." + exception);
        }
        return flag;
    }

    public File getCustomResImage(Bitmap b, String fileName) {

        try {
            //final int destWidth = 720;//or the width you need
            //final int destHeight = 1024;//or the height you need

            // we create an scaled bitmap so it reduces the image, not just trim it
            //Bitmap b2 = Bitmap.createScaledBitmap(b, destWidth, destHeight, false);
            ByteArrayOutputStream outStream = new ByteArrayOutputStream();
            // compress to the format you want, JPEG, PNG...
            // 70 is the 0-100 quality percentage
            b.compress(Bitmap.CompressFormat.JPEG,100 , outStream);
            // we save the file, at least until we have made use of it
            File f = new File(context.getCacheDir(), fileName);

            if (f.createNewFile()) {
                //write the bytes in file
                FileOutputStream fo = new FileOutputStream(f);
                fo.write(outStream.toByteArray());
                // remember close de FileOutput
                fo.close();

                return f;
            } else {
                return null;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
