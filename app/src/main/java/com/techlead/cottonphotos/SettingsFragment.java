package com.techlead.cottonphotos;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ListObjectsRequest;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.amazonaws.util.DateUtils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.apache.commons.io.FilenameUtils;

import java.io.File;
import java.io.FileWriter;
import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

public class SettingsFragment extends Fragment {

    private Context context;
    private LinearLayout layParent;

    private EditText username, phoneNumber, cityName, photoNamePrefix, category;
    private TextView oldDataMessage;
    private LinearLayout userDataLayout;
    private Button saveDetailsButton, syncButton,resetButton;
    private LinearLayout dataSyncLayout;
    private ArrayList<ImageData> filePathArrayList;
    private ProgressDialog progDailog;
    private int uploadCount = 0;
    private int totalUnSyncedFile = 0;
    private TextView tvDataSync;
    private String blockCharacterSet = "~#^|$%&*! (){}[]@_-+/=<>,.\\";

    InputFilter filter = new InputFilter() {
        public CharSequence filter(CharSequence source, int start, int end,
                                   Spanned dest, int dstart, int dend) {
            for (int i = start; i < end; i++) {
                if (blockCharacterSet.contains("" + source.charAt(i))) {
                    return "";
                }
            }
            return null;
        }
    };


    public SettingsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_settings, container, false);

        context = getActivity();
        filePathArrayList = new ArrayList<>();

        username = view.findViewById(R.id.username);
        username.setFilters(new InputFilter[] { filter });
        phoneNumber = view.findViewById(R.id.phoneNumber);
        phoneNumber.setFilters(new InputFilter[] { filter });
        cityName = view.findViewById(R.id.cityName);
        cityName.setFilters(new InputFilter[] { filter });
        photoNamePrefix = view.findViewById(R.id.photoNamePrefix);
        photoNamePrefix.setFilters(new InputFilter[] { filter });
        category = view.findViewById(R.id.category);
        category.setFilters(new InputFilter[] { filter });
        syncButton = view.findViewById(R.id.syncButton);
        resetButton = view.findViewById(R.id.resetButton);
        dataSyncLayout = view.findViewById(R.id.dataSyncLayout);
        oldDataMessage = view.findViewById(R.id.oldDataMessage);
        userDataLayout = view.findViewById(R.id.userDataLayout);

        saveDetailsButton = view.findViewById(R.id.saveDetailsButton);

        tvDataSync = view.findViewById(R.id.tvDataSync);


        final SharedPreferences sharedPreferences = context.getSharedPreferences("User", 0);

        String oldUsername = sharedPreferences.getString("username", "");
        String oldPhoneNumber = sharedPreferences.getString("phoneNumber", "");
        String oldCityName = sharedPreferences.getString("cityName", "");
        String oldPhotoNamePrefix = sharedPreferences.getString("photoNamePrefix", "");
        String oldCategory = sharedPreferences.getString("category", "");

        username.setText(oldUsername);
        phoneNumber.setText(oldPhoneNumber);
        cityName.setText(oldCityName);
        photoNamePrefix.setText(oldPhotoNamePrefix);
        category.setText(oldCategory);

        saveDetailsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String newUsername = username.getText().toString();
                String newPhoneNumber = phoneNumber.getText().toString();
                String newCityName = cityName.getText().toString();
                String newPhotoNamePrefix = photoNamePrefix.getText().toString();
                String newCategory = category.getText().toString();

                sharedPreferences.edit().putString("username", newUsername).commit();
                sharedPreferences.edit().putString("phoneNumber", newPhoneNumber).commit();
                sharedPreferences.edit().putString("cityName", newCityName).commit();
                sharedPreferences.edit().putString("photoNamePrefix", newPhotoNamePrefix).commit();
                sharedPreferences.edit().putString("category", newCategory).commit();

                Thread t1 = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        uploadMetaFilesToS3();
                    }});
                t1.start();

                new AlertDialog.Builder(context)
                        .setTitle("Cotton Photos")
                        .setCancelable(false)
                        .setMessage("Details saved successfully.")
                        .setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        }).show();
            }
        });

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        context = getActivity();

        SharedPreferences sharedPreferences = context.getSharedPreferences("User", 0);

        String storedUsername = sharedPreferences.getString("username", "");
        String storedCategory = sharedPreferences.getString("category", "");

//        if (!storedUsername.equals("") && !storedCategory.equals("")) {
            String filePathsString = sharedPreferences.getString("imagepath", "");

            if (!filePathsString.equals("")) {

                dataSyncLayout.setVisibility(View.VISIBLE);
                oldDataMessage.setVisibility(View.VISIBLE);

                //setViewAndChildrenEnabled(userDataLayout, false);

                Type type = new TypeToken<ArrayList<ImageData>>() {}.getType();
                Gson gson = new Gson();
                filePathArrayList = gson.fromJson(filePathsString, type);
                totalUnSyncedFile = filePathArrayList.size();

                tvDataSync.setText("Data Sync - " + totalUnSyncedFile + " files pending");

                resetButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        getActivity().runOnUiThread(new Runnable() {
                            public void run() {

                                new AlertDialog.Builder(context)
                                        .setTitle("Cotton Photos")
                                        .setCancelable(true)
                                        .setMessage("Are you sure you want to RESET photos ?")
                                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                // Clear SP
                                                SharedPreferences sharedPreferences = context.getSharedPreferences("User", 0);
                                                String keyForImages = "imagepath";

                                                sharedPreferences.edit().putString(keyForImages, "").commit();

                                                onResume();

                                                dialog.dismiss();
                                            }
                                        }).show();
                            }
                        });
                    }
                });

                syncButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        if (checkConnection(context)) {
                            syncDataWithServer(filePathArrayList);
                        } else {

                            getActivity().runOnUiThread(new Runnable() {
                                public void run() {

                                    new AlertDialog.Builder(context)
                                            .setTitle("Cotton Photos")
                                            .setCancelable(true)
                                            .setMessage("You need to have internet connection to perform this task.")
                                            .setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    dialog.dismiss();
                                                }
                                            }).show();
                                }
                            });
                        }
                    }
                });

            } else {

                dataSyncLayout.setVisibility(View.GONE);
                oldDataMessage.setVisibility(View.GONE);

                //setViewAndChildrenEnabled(userDataLayout, true);
            }

    }

    private static void setViewAndChildrenEnabled(View view, boolean enabled) {
        view.setEnabled(enabled);
        if (view instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) view;
            for (int i = 0; i < viewGroup.getChildCount(); i++) {
                View child = viewGroup.getChildAt(i);
                setViewAndChildrenEnabled(child, enabled);
            }
        }
    }

    private void syncDataWithServer(final ArrayList<ImageData> filePathArrayList) {

        Thread t1 = new Thread(new Runnable() {
            @Override
            public void run() {

                uploadCount = 0;

                getActivity().runOnUiThread(new Runnable() {
                    public void run() {
                        progDailog = new ProgressDialog(context);
                        progDailog.setMessage("Uploading files to server...");
                        progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                        progDailog.setCancelable(false);
                        progDailog.setIndeterminate(false);
                        progDailog.show();
                    }
                });


                for (int i=0; i<filePathArrayList.size(); i++) {

                    final int finalI = i;
                    getActivity().runOnUiThread(new Runnable() {
                        public void run() {

                            progDailog.setMessage("Uploading " + (finalI +1) + " out of " + filePathArrayList.size());
                        }
                    });

                    if (filePathArrayList.get(i) == null) {
                        continue;
                    }

                    String[] pathArray = filePathArrayList.get(i).getPath().split("/");
                    String displayName = pathArray[pathArray.length - 1];

                    SimpleDateFormat ft = new SimpleDateFormat ("yyyy.MM.dd-hh:mm:ss:SSS");
                    String ext = FilenameUtils.getExtension(displayName);

                    SharedPreferences sharedPreferences = context.getSharedPreferences("User", 0);
                    //String photoNamePrefix = sharedPreferences.getString("photoNamePrefix", "");
                    //String category = sharedPreferences.getString("category", "");

//                    if (photoNamePrefix.equals("")) {
//                        photoNamePrefix = "CottonPhotos";
//                    }
//
//                    if (category.equals("")) {
//                        category = "General";
//                    }

                    final String fileName = filePathArrayList.get(i).getName();//category.concat("_").concat(photoNamePrefix).concat("_").concat(ft.format(new Date())).replace(" ", "_");

                    boolean status = uploadFilesToS3(filePathArrayList.get(i).getPath(), fileName, ext);

                    if (status) {

                        uploadCount++;
                    }
                }

                getActivity().runOnUiThread(new Runnable() {
                    public void run() {

                        if (progDailog != null) {
                            progDailog.dismiss();
                        }

                    }
                });

                if (uploadCount == filePathArrayList.size()) {

                    SharedPreferences sharedPreferences = context.getSharedPreferences("User", 0);
                    sharedPreferences.edit().putString("imagepath", "").commit();

                    String username = sharedPreferences.getString("username", "");
                    String category = sharedPreferences.getString("category", "");

                    String keyForImages = username.concat("_").concat(category).concat("images");
                    sharedPreferences.edit().putString(keyForImages, "").commit();


                    getActivity().runOnUiThread(new Runnable() {
                        public void run() {

                            Toast.makeText(context, "Data synced successfully with server.", Toast.LENGTH_SHORT).show();
                            dataSyncLayout.setVisibility(View.GONE);
                        }
                    });

                } else {

                    getActivity().runOnUiThread(new Runnable() {
                        public void run() {

                            Toast.makeText(context, "Error occurred while uploading data to server. Please try again later.", Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }
        });
        t1.start();
    }

    public boolean uploadMetaFilesToS3(){

        try {

            ClientConfiguration clientConfiguration = new ClientConfiguration()
                    .withMaxErrorRetry(1) // 1 retries
                    .withConnectionTimeout(200000) // 30,000 ms
                    .withSocketTimeout(200000); // 30,000 ms

            AmazonS3 s3Client = new AmazonS3Client(new BasicAWSCredentials("AKIAJVLYTY7JZ5HHOCTQ", "v2IuBHv/ritpqr0/pbPCKqxt7XBWH6bLGrIEmupD"), clientConfiguration);
            s3Client.setRegion(Region.getRegion(Regions.AP_SOUTH_1));

            SharedPreferences sharedPreferences = context.getSharedPreferences("User", 0);
            String username = sharedPreferences.getString("username", "");
            String photoNamePrefix = sharedPreferences.getString("photoNamePrefix", "");
            String category = sharedPreferences.getString("category", "");
            String phoneNumber = sharedPreferences.getString("phoneNumber", "");
            String cityName = sharedPreferences.getString("cityName", "");


            String folderName = "";

            if (username.equals("")) {
                folderName = "CottonPhotos";
            } else {
                folderName = username;
            }
            String filePath = "";

            File gpxfile = File.createTempFile("meta","meta");
            FileWriter writer = new FileWriter(gpxfile);
            writer.append("username:").append(username).append(" | ");
            writer.append("photoNamePrefix:").append(photoNamePrefix).append(" | ");
            writer.append("category:").append(category).append(" | ");
            writer.append("phoneNumber:").append(phoneNumber).append(" | ");
            writer.append("cityName:").append(cityName).append(" | ");
            writer.flush();
            writer.close();


            PutObjectRequest por = new PutObjectRequest("farmai", folderName.concat("/metafile"), gpxfile);
            por.withCannedAcl(CannedAccessControlList.PublicRead);

            s3Client.putObject(por);
            return true;
        } catch (Exception e){
            return false;
        }
    }

    public boolean uploadFilesToS3(String filePath, String fileName, String ext){

        try {

            ClientConfiguration clientConfiguration = new ClientConfiguration()
                    .withMaxErrorRetry(1) // 1 retries
                    .withConnectionTimeout(200000) // 30,000 ms
                    .withSocketTimeout(200000); // 30,000 ms

            AmazonS3 s3Client = new AmazonS3Client(new BasicAWSCredentials("AKIAJVLYTY7JZ5HHOCTQ", "v2IuBHv/ritpqr0/pbPCKqxt7XBWH6bLGrIEmupD"), clientConfiguration);
            s3Client.setRegion(Region.getRegion(Regions.AP_SOUTH_1));

            SharedPreferences sharedPreferences = context.getSharedPreferences("User", 0);
            String username = sharedPreferences.getString("username", "");
            String photoNamePrefix = sharedPreferences.getString("photoNamePrefix", "");
            String category = sharedPreferences.getString("category", "");
            String phoneNumber = sharedPreferences.getString("phoneNumber", "");
            String cityName = sharedPreferences.getString("cityName", "");

            String folderName = "";

            if (username.equals("")) {
                folderName = "CottonPhotos";
            } else {
                folderName = username;
            }

            ListObjectsRequest listObjectsRequest = new ListObjectsRequest()
                    .withBucketName("farmai")
                    .withPrefix(folderName + "/");

            ObjectListing objectListing = s3Client.listObjects(listObjectsRequest);

//            int count = 0;
//            Date lastModifiedDate = null;
//            Date currentDate = new Date();
//            String objectNumber = "";
//
//            Calendar cal1 = Calendar.getInstance();
//            Calendar cal2 = Calendar.getInstance();
//            cal1.setTimeZone(TimeZone.getDefault());
//            cal2.setTimeZone(TimeZone.getDefault());
//
//            cal2.setTime(currentDate);

//            for (S3ObjectSummary summary : objectListing.getObjectSummaries()) {
//
//                if (lastModifiedDate == null) {
//
//                    lastModifiedDate = summary.getLastModified();
//                } else if (summary.getLastModified().after(lastModifiedDate)){
//
//                    lastModifiedDate = summary.getLastModified();
//                }
//
//                cal1.setTime(lastModifiedDate);
//
//                boolean sameDay = cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR) &&
//                        cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR);
//
//                if (sameDay) {
//                    count++ ;
//                }
//            }

//            if (lastModifiedDate != null) {
//                cal1.setTime(lastModifiedDate);
//                cal2.setTime(currentDate);
//                boolean sameDay = cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR) &&
//                        cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR);
//
//                objectNumber = String.format(Locale.getDefault(), "%04d", count);
//
//                if (!sameDay) {
//
//                    objectNumber = "0000";
//                }
//            } else {
//                objectNumber = "0000";
//            }

            Map<String,String> map = new HashMap<>();
            map.put("U",username);
            map.put("P",photoNamePrefix);
            map.put("N",phoneNumber);
            map.put("C",category);
            map.put("L",cityName);

            PutObjectRequest por = new PutObjectRequest("farmai", folderName.concat("/").concat(fileName.concat(".").concat(ext)), new File(filePath));
            por.withCannedAcl(CannedAccessControlList.PublicRead);
            ObjectMetadata metadata = new ObjectMetadata();
            metadata.setUserMetadata(map);
            por.withMetadata(metadata);
            s3Client.putObject(por);
            return true;
        } catch (Exception e){
            return false;
        }
    }

    public boolean checkConnection(Context context) {
        boolean flag = false;
        try {
            ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            if(connectivityManager != null){
                NetworkInfo info = connectivityManager.getActiveNetworkInfo();

                if(info != null){
                    if (info.getType() == ConnectivityManager.TYPE_WIFI) {
                        System.out.println(info.getTypeName());
                        flag = true;
                    }
                    if (info.getType() == ConnectivityManager.TYPE_MOBILE) {
                        System.out.println(info.getTypeName());
                        flag = true;
                    }
                }
            }

        } catch (Exception exception) {
            System.out.println("Exception at network connection....." + exception);
        }
        return flag;
    }

}
